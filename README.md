GralphQL Experiment
===================

Based on:
https://www.graphql-java.com/documentation/v12/getting-started/

Check if there is a way to build a Database Query depending on the parameters requested by GraphQL.

This Blog: [Building efficient data fetchers by looking ahead](https://www.graphql-java.com/blog/deep-dive-data-fetcher-results/) describe what I have tryed to invent too.