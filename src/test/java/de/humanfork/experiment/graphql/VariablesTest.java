package de.humanfork.experiment.graphql;

import java.util.Map;

import org.junit.jupiter.api.Test;

import de.humanfork.experiment.graphql.asserts.ExecutionResultAssert;
import graphql.ExecutionInput;
import graphql.ExecutionResult;
import graphql.GraphQL;
import graphql.schema.GraphQLSchema;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.SchemaGenerator;
import graphql.schema.idl.SchemaParser;
import graphql.schema.idl.TypeDefinitionRegistry;

/**
 * It wouldn't be a good idea to pass these dynamic arguments directly in the query string,
 * because then our client-side code would need to dynamically manipulate the query string at runtime,
 * and serialize it into a GraphQL-specific format
 *
 * Instead, GraphQL has a first-class way to factor dynamic values out of the query,
 * and pass them as a separate dictionary.
 */
public class VariablesTest {

    public GraphQL buildGraphQlApi() {
        String schema = "type Query {calcSum(a:Int!, b:Int!): Int! }";

        SchemaParser schemaParser = new SchemaParser();
        TypeDefinitionRegistry typeDefinitionRegistry = schemaParser.parse(schema);

        RuntimeWiring runtimeWiring = RuntimeWiring.newRuntimeWiring()
                .type("Query",
                        builder -> builder.dataFetcher("calcSum",
                                environment -> ((Integer) environment.getArgument("a"))
                                        + ((Integer) environment.getArgument("b"))))
                .build();

        SchemaGenerator schemaGenerator = new SchemaGenerator();
        GraphQLSchema graphQLSchema = schemaGenerator.makeExecutableSchema(typeDefinitionRegistry, runtimeWiring);

        return GraphQL.newGraphQL(graphQLSchema).build();
    }

    @Test
    public void testPlain() {
        ExecutionResultAssert.assertThat(buildGraphQlApi().execute("query sum {calcSum(a:1 b:2)}"))
                .isDataEqualTo("{calcSum=3}");
    }

    @Test
    public void testVariableSeperate() {

        ExecutionInput executionInput = ExecutionInput
                .newExecutionInput("query sum($varA:Int! $varB:Int!) {calcSum(a:$varA b:$varB)}")
                .variables(Map.of("varA", 1, "varB", 2)).build();

        ExecutionResult result = buildGraphQlApi().execute(executionInput);
        ExecutionResultAssert.assertThat(result).isDataEqualTo("{calcSum=3}");
    }

    @Test
    public void testDefaultVariable() {

        ExecutionInput executionInput = ExecutionInput
                .newExecutionInput("query sum($varA:Int=1 $varB:Int=2) {calcSum(a:$varA b:$varB)}")
                .variables(Map.of("varA", 10)).build();

        ExecutionResult result = buildGraphQlApi().execute(executionInput);
        ExecutionResultAssert.assertThat(result).isDataEqualTo("{calcSum=12}");
    }

}
