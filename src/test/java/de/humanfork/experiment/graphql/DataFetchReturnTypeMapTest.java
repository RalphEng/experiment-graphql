package de.humanfork.experiment.graphql;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Map;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import de.humanfork.experiment.graphql.asserts.ExecutionResultAssert;
import de.humanfork.experiment.graphql.asserts.GraphQLObjectTypeAssert;
import graphql.ExecutionResult;
import graphql.GraphQL;
import graphql.execution.NonNullableFieldWasNullError;
import graphql.schema.GraphQLFieldDefinition;
import graphql.schema.GraphQLSchema;
import graphql.schema.StaticDataFetcher;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.SchemaGenerator;
import graphql.schema.idl.SchemaParser;
import graphql.schema.idl.TypeDefinitionRegistry;
import graphql.validation.ValidationError;

/**
 * Test how GraphQL behaves if the {@link  graphql.schema.DataFetcher} return a map instead of a "normal" typed object. 
 * 
 * @author Ralph Engelmann
 */
public class DataFetchReturnTypeMapTest {

    /**
     * Example: The DataFetcher return a Map with attributes that match the schema
     * as well as the query
     */
    @Test
    public void testMapFetchExample() {
        String schema = "type Query{info: Info!} \n" + "type Info {id:ID, text:String} ";

        SchemaParser schemaParser = new SchemaParser();
        TypeDefinitionRegistry typeDefinitionRegistry = schemaParser.parse(schema);

        RuntimeWiring runtimeWiring = RuntimeWiring.newRuntimeWiring()
                .type("Query",
                        builder -> builder.dataFetcher("info", new StaticDataFetcher(Map.of("id", 1, "text", "hello"))))
                .build();

        SchemaGenerator schemaGenerator = new SchemaGenerator();
        GraphQLSchema graphQLSchema = schemaGenerator.makeExecutableSchema(typeDefinitionRegistry, runtimeWiring);

        /* check the schema */
        assertThat(graphQLSchema.getQueryType()).isNotNull();
        assertThat(graphQLSchema.getQueryType().getFieldDefinitions()).extracting(GraphQLFieldDefinition::getName)
                .containsExactly("info");

        GraphQLObjectTypeAssert.assertThat(graphQLSchema.getQueryType()).hasFieldDefinition("info",
                a -> a.isOfType("Info!").hasNoArgurments());

        /* execute query for hello (without parameters) */
        GraphQL build = GraphQL.newGraphQL(graphQLSchema).build();
        ExecutionResult executionResult = build.execute("{info{id, text}}");

        ExecutionResultAssert.assertThat(executionResult).isDataEqualTo("{info={id=1, text=hello}}");
    }

    @Nested
    public class NotMatchingFieldSets {

        /**
         * Example: The DataFetcher return a Map with attributes that match the schema
         * but the query select only a subset
         */
        @Test
        public void testMapFetchExampleSelectSubset() {
            String schema = "type Query{info: Info!} \n" + "type Info {id:ID, text:String} ";

            SchemaParser schemaParser = new SchemaParser();
            TypeDefinitionRegistry typeDefinitionRegistry = schemaParser.parse(schema);

            RuntimeWiring runtimeWiring = RuntimeWiring.newRuntimeWiring()
                    .type("Query",
                            builder -> builder.dataFetcher("info",
                                    new StaticDataFetcher(Map.of("id", 1, "text", "hello"))))
                    .build();

            SchemaGenerator schemaGenerator = new SchemaGenerator();
            GraphQLSchema graphQLSchema = schemaGenerator.makeExecutableSchema(typeDefinitionRegistry, runtimeWiring);

            /* check the schema */
            assertThat(graphQLSchema.getQueryType()).isNotNull();
            assertThat(graphQLSchema.getQueryType().getFieldDefinitions()).extracting(GraphQLFieldDefinition::getName)
                    .containsExactly("info");

            GraphQLObjectTypeAssert.assertThat(graphQLSchema.getQueryType()).hasFieldDefinition("info",
                    a -> a.isOfType("Info!").hasNoArgurments());

            /* execute query for hello (without parameters) */
            GraphQL build = GraphQL.newGraphQL(graphQLSchema).build();
            ExecutionResult executionResult = build.execute("{info{id}}");

            ExecutionResultAssert.assertThat(executionResult).isDataEqualTo("{info={id=1}}");
        }

        /**
         * Example: The DataFetcher return a Map with attributes that miss the "text"
         * attribute that is required by the schema as well as the query
         */
        @Test
        public void testMapFetchExampleDataFetcherProvidesLess() {
            String schema = "type Query{info: Info!} \n" + "type Info {id:ID, text:String} ";

            SchemaParser schemaParser = new SchemaParser();
            TypeDefinitionRegistry typeDefinitionRegistry = schemaParser.parse(schema);

            RuntimeWiring runtimeWiring = RuntimeWiring.newRuntimeWiring()
                    .type("Query",
                            builder -> builder.dataFetcher("info", new StaticDataFetcher(Map.of("id", 1))))
                    .build();

            SchemaGenerator schemaGenerator = new SchemaGenerator();
            GraphQLSchema graphQLSchema = schemaGenerator.makeExecutableSchema(typeDefinitionRegistry, runtimeWiring);

            /* check the schema */
            assertThat(graphQLSchema.getQueryType()).isNotNull();
            assertThat(graphQLSchema.getQueryType().getFieldDefinitions()).extracting(GraphQLFieldDefinition::getName)
                    .containsExactly("info");

            GraphQLObjectTypeAssert.assertThat(graphQLSchema.getQueryType()).hasFieldDefinition("info",
                    a -> a.isOfType("Info!").hasNoArgurments());

            /* execute query for hello (without parameters) */
            GraphQL build = GraphQL.newGraphQL(graphQLSchema).build();
            ExecutionResult executionResult = build.execute("{info{id, text}}");

            ExecutionResultAssert.assertThat(executionResult).isDataEqualTo("{info={id=1, text=null}}");
        }

        /**
         * Example: The DataFetcher return a Map with attributes that miss the "text"
         * attribute that is required by the schema as well as the query
         */
        @Test
        public void testMapFetchExampleDataFetcherProvidesLessMissingAttributeIsRequired() {
            String schema = "type Query{info: Info!} \n" + "type Info {id:ID, text:String!} ";

            SchemaParser schemaParser = new SchemaParser();
            TypeDefinitionRegistry typeDefinitionRegistry = schemaParser.parse(schema);

            RuntimeWiring runtimeWiring = RuntimeWiring.newRuntimeWiring()
                    .type("Query",
                            builder -> builder.dataFetcher("info", new StaticDataFetcher(Map.of("id", 1))))
                    .build();

            SchemaGenerator schemaGenerator = new SchemaGenerator();
            GraphQLSchema graphQLSchema = schemaGenerator.makeExecutableSchema(typeDefinitionRegistry, runtimeWiring);

            /* check the schema */
            assertThat(graphQLSchema.getQueryType()).isNotNull();
            assertThat(graphQLSchema.getQueryType().getFieldDefinitions()).extracting(GraphQLFieldDefinition::getName)
                    .containsExactly("info");

            GraphQLObjectTypeAssert.assertThat(graphQLSchema.getQueryType()).hasFieldDefinition("info",
                    a -> a.isOfType("Info!").hasNoArgurments());

            /* execute query for hello (without parameters) */
            GraphQL build = GraphQL.newGraphQL(graphQLSchema).build();
            ExecutionResult executionResult = build.execute("{info{id, text}}");

            ExecutionResultAssert.assertThat(executionResult).hasError(NonNullableFieldWasNullError.class);
        }

        /**
         * Example: The DataFetcher return a Map more attributes than the match the
         * schema and the query select
         */
        @Test
        public void testMapFetchExampleDataFetcherMoreAttributeThanSchemaAndQuery() {
            String schema = "type Query{info: Info!} \n" + "type Info {id:ID} ";

            SchemaParser schemaParser = new SchemaParser();
            TypeDefinitionRegistry typeDefinitionRegistry = schemaParser.parse(schema);

            RuntimeWiring runtimeWiring = RuntimeWiring.newRuntimeWiring()
                    .type("Query",
                            builder -> builder.dataFetcher("info",
                                    new StaticDataFetcher(Map.of("id", 1, "text", "hello"))))
                    .build();

            SchemaGenerator schemaGenerator = new SchemaGenerator();
            GraphQLSchema graphQLSchema = schemaGenerator.makeExecutableSchema(typeDefinitionRegistry, runtimeWiring);

            /* check the schema */
            assertThat(graphQLSchema.getQueryType()).isNotNull();
            assertThat(graphQLSchema.getQueryType().getFieldDefinitions()).extracting(GraphQLFieldDefinition::getName)
                    .containsExactly("info");

            GraphQLObjectTypeAssert.assertThat(graphQLSchema.getQueryType()).hasFieldDefinition("info",
                    a -> a.isOfType("Info!").hasNoArgurments());

            /* execute query for hello (without parameters) */
            GraphQL build = GraphQL.newGraphQL(graphQLSchema).build();
            ExecutionResult executionResult = build.execute("{info{id}}");

            ExecutionResultAssert.assertThat(executionResult).isDataEqualTo("{info={id=1}}");
        }

        /**
         * Example: The DataFetcher return a Map more attributes than the match the
         * schema, but the query select the extra attribute
         */
        @Test
        public void testMapFetchExampleDataFetcherAndQueryMoreAttributeThanSchema() {
            String schema = "type Query{info: Info!} \n" + "type Info {id:ID} ";

            SchemaParser schemaParser = new SchemaParser();
            TypeDefinitionRegistry typeDefinitionRegistry = schemaParser.parse(schema);

            RuntimeWiring runtimeWiring = RuntimeWiring.newRuntimeWiring()
                    .type("Query",
                            builder -> builder.dataFetcher("info",
                                    new StaticDataFetcher(Map.of("id", 1, "text", "hello"))))
                    .build();

            SchemaGenerator schemaGenerator = new SchemaGenerator();
            GraphQLSchema graphQLSchema = schemaGenerator.makeExecutableSchema(typeDefinitionRegistry, runtimeWiring);

            /* check the schema */
            assertThat(graphQLSchema.getQueryType()).isNotNull();
            assertThat(graphQLSchema.getQueryType().getFieldDefinitions()).extracting(GraphQLFieldDefinition::getName)
                    .containsExactly("info");

            GraphQLObjectTypeAssert.assertThat(graphQLSchema.getQueryType()).hasFieldDefinition("info",
                    a -> a.isOfType("Info!").hasNoArgurments());

            /* execute query for hello (without parameters) */
            GraphQL build = GraphQL.newGraphQL(graphQLSchema).build();
            ExecutionResult executionResult = build.execute("{info{id, text}}");

            ExecutionResultAssert.assertThat(executionResult).hasError(ValidationError.class);
        }
    }
}
