package de.humanfork.experiment.graphql;

import java.util.Optional;

import org.junit.jupiter.api.Test;

import de.humanfork.experiment.graphql.asserts.ExecutionResultAssert;
import graphql.GraphQL;
import graphql.schema.GraphQLSchema;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.SchemaGenerator;
import graphql.schema.idl.SchemaParser;
import graphql.schema.idl.TypeDefinitionRegistry;

/**
 * GraphQL Schema has the concept that some types are nullable and others are not.
 * In Java this is done by using {@link Optional}.
 *
 * This test check what happen if a DataFetcher returns an {@link Optional}.
 * 
 * result:
 * So it seams that GraphQL Java Implementation automatic unwarp the {@link Optional}.
 * 
 * @author Ralph Engelmann
 */
public class DataFetchReturnTypeOptional {

    public GraphQL buildGraphQlApi() {
        String schema = "type Query{findById(id:Int!): String}";

        SchemaParser schemaParser = new SchemaParser();
        TypeDefinitionRegistry typeDefinitionRegistry = schemaParser.parse(schema);

        RuntimeWiring runtimeWiring = RuntimeWiring.newRuntimeWiring()
                .type("Query", builder -> builder.dataFetcher("findById", environment -> {
                    Integer idArgument = environment.getArgument("id");
                    if (idArgument.equals(1)) {
                        return Optional.of("found");
                    } else {
                        return Optional.<String> empty();
                    }
                })).build();

        SchemaGenerator schemaGenerator = new SchemaGenerator();
        GraphQLSchema graphQLSchema = schemaGenerator.makeExecutableSchema(typeDefinitionRegistry, runtimeWiring);

        return GraphQL.newGraphQL(graphQLSchema).build();
    }

    @Test
    public void testOptionalExist() {
        /* execute query for existing id*/
        ExecutionResultAssert.assertThat(buildGraphQlApi().execute("{findById(id:1)}"))
                .isDataEqualTo("{findById=found}");
    }

    @Test
    public void testOptionalEmpty() {
        /* execute query for not existing id*/
        ExecutionResultAssert.assertThat(buildGraphQlApi().execute("{findById(id:0)}"))
                .isDataEqualTo(("{findById=null}"));
    }
}
