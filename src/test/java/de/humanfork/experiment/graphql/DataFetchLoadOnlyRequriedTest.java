package de.humanfork.experiment.graphql;

import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import de.humanfork.experiment.graphql.asserts.ExecutionResultAssert;
import de.humanfork.experiment.graphql.cowdomain.Cow;
import de.humanfork.experiment.graphql.cowdomain.CowRepository;
import de.humanfork.experiment.graphql.cowdomain.CowResidence;
import de.humanfork.experiment.graphql.cowdomain.DateRange;
import de.humanfork.experiment.graphql.cowdomain.Stable;
import de.humanfork.experiment.graphql.util.Pair;
import graphql.ExecutionResult;
import graphql.GraphQL;
import graphql.execution.DataFetcherResult;
import graphql.execution.DataFetcherResult.Builder;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.GraphQLSchema;
import graphql.schema.SelectedField;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.SchemaGenerator;
import graphql.schema.idl.SchemaParser;
import graphql.schema.idl.TypeDefinitionRegistry;

/**
 * Test how to reduced the entity graph loaded from the database based on the GraphQL-Selection-Set.
 *
 * In this test the loaded entity graph is scaled by using a join not not join:
 * - small: select * from A;
 * - big: select * from A join B
 *
 * Imagine that the join is very expensive and should been done only if the data from B is needed.
 * But on the other hand a naive first Load A and then load B result in a N+1 problem.
 * Therefore this experiment show how to use joins but only when it is needed.
 * 
 * This test contains of two different solutions:
 * - the more easier one return an extra container object, containing A and B
 * - the more complex one "inject" B into A 
 *
 * @author engelmann
 *
 */
public class DataFetchLoadOnlyRequriedTest {

    private static final LocalDate NOW = LocalDate.of(2019, Month.AUGUST, 2);

    public static class Util {
        public static LocalDate parseDateArgument(final String s) {
            if (s.equalsIgnoreCase("NOW")) {
                return NOW;
            }
            if (s.equalsIgnoreCase("YESTERDAY")) {
                return NOW.minusDays(1);
            } else {
                return LocalDate.parse(s, DateTimeFormatter.ISO_DATE);
            }
        }
    }

    private static CowRepository buildCowRepository() {
        Cow friggy = new Cow(1, "Friggy");
        Cow birgit = new Cow(2, "Birgit");

        Stable dresden = new Stable(1, "Dresden");
        Stable freital = new Stable(2, "Freital");
        Stable hamburg = new Stable(2, "Hamburg");

        List<CowResidence> cowResisdence = Arrays.asList(
                new CowResidence(friggy, hamburg, new DateRange(NOW.minusDays(10), NOW.minusDays(5))),
                new CowResidence(friggy, freital, new DateRange(NOW.minusDays(5), NOW)),
                new CowResidence(friggy, dresden, new DateRange(NOW, NOW.plusDays(10))),

                new CowResidence(birgit, dresden, new DateRange(NOW.minusDays(10), NOW.minusDays(5))),
                new CowResidence(birgit, freital, new DateRange(NOW.minusDays(5), NOW)),
                new CowResidence(birgit, hamburg, new DateRange(NOW, NOW.plusDays(10))));

        return new CowRepository(cowResisdence);
    }

    /**
     * Use an extra container {@link CowResult} to provide {@link Cow} and {@link Stable}.
     */
    @Nested
    public static class ExtraContainerTest {

        // @formatter:off
        private static final String SCHEMA =
             "type Query{cows: [CowResult!]!} \n"
           + "type CowResult{cow:Cow!, stable(date:String=\"NOW\"):Stable!} \n"
           + "type Cow {id:ID!, name:String} \n"
           + "type Stable {id:ID!, name:String} \n";
        // @formatter:on

        /** Helper class to combine cow and stable (at a secific point in time) for GraphQL query result. */
        public static class CowResult {

            @SuppressWarnings("unused")
            private final Cow cow;

            /** This become the stable at a specific point in time (argument of stable() function) */
            //can be null
            @SuppressWarnings("unused")
            private final Stable stable;

            public CowResult(final Cow cow, final Optional<Stable> stable) {
                this.cow = cow;
                this.stable = stable.orElse(null);
            }

            public CowResult(final Cow cow) {
                this(cow, Optional.empty());
            }
        }

        /**
         * Fetch a cow and its current residence.
         *
         * This Data Fetcher return for each cow, the cow itselfe and the stable where it resists.
         * The point in time for the residence is take from the argument "date" of stable function.
         *
         * The important point is, that the cow-stable information is only loaded when the "stable"
         * is in the SelectionSet.
         */
        public static class CowDataFetcher implements DataFetcher<List<CowResult>> {

            private final CowRepository cowRepository;

            public CowDataFetcher(final CowRepository cowRepository) {
                this.cowRepository = cowRepository;
            }

            @Override
            public List<CowResult> get(final DataFetchingEnvironment environment) throws Exception {

                boolean requiresResidence = environment.getSelectionSet().contains("stable/**");

                if (requiresResidence) {
                    SelectedField field = environment.getSelectionSet().getField("stable");
                    assert (field != null);
                    Optional<String> dateArgument = Optional.ofNullable((String) field.getArguments().get("date"));
                    LocalDate date = dateArgument.map(Util::parseDateArgument).orElse(NOW);

                    return this.cowRepository.findAllAndStableAtDate(date)
                            .sorted(Comparator.<Pair<Cow, ?>, Cow> comparing(pair -> pair.getLeft(),
                                    Cow.COW_BY_ID_COMPARATOR))
                            .map(pair -> new CowResult(pair.getLeft(), pair.getRight())).collect(Collectors.toList());
                } else {
                    return this.cowRepository.findAllAsStream().sorted(Cow.COW_BY_ID_COMPARATOR).map(CowResult::new)
                            .collect(Collectors.toList());
                }
            }
        }

        private GraphQL initGraphQl(final CowRepository cowRepository) {
            SchemaParser schemaParser = new SchemaParser();
            TypeDefinitionRegistry typeDefinitionRegistry = schemaParser.parse(SCHEMA);

            RuntimeWiring runtimeWiring = RuntimeWiring.newRuntimeWiring()
                    .type("Query", builder -> builder.dataFetcher("cows", new CowDataFetcher(cowRepository))).build();

            SchemaGenerator schemaGenerator = new SchemaGenerator();
            GraphQLSchema graphQLSchema = schemaGenerator.makeExecutableSchema(typeDefinitionRegistry, runtimeWiring);

            return GraphQL.newGraphQL(graphQLSchema).build();
        }

        @Test
        public void testLoadCowOnly() {
            GraphQL graphQL = initGraphQl(buildCowRepository());

            /* execute query for hello (without parameters) */
            ExecutionResult executionResult = graphQL.execute("{cows{cow{id, name}}}");
            ExecutionResultAssert.assertThat(executionResult)
                    .isDataEqualTo("{cows=[{cow={id=1, name=Friggy}}, {cow={id=2, name=Birgit}}]}");
        }

        @Test
        public void testLoadCowWithStableNow() {
            GraphQL graphQL = initGraphQl(buildCowRepository());

            /* execute query for hello (without parameters) */
            ExecutionResult executionResult = graphQL.execute("{cows{cow{id, name}, stable(date:\"NOW\"){name}}}");
            ExecutionResultAssert.assertThat(executionResult).isDataEqualTo(
                    "{cows=[{cow={id=1, name=Friggy}, stable={name=Dresden}}, {cow={id=2, name=Birgit}, stable={name=Hamburg}}]}");
        }

        @Test
        public void testLoadCowWithStableYesterDay() {
            GraphQL graphQL = initGraphQl(buildCowRepository());

            /* execute query for hello (without parameters) */
            ExecutionResult executionResult = graphQL
                    .execute("{cows{cow{id, name}, stable(date:\"YESTERDAY\"){name}}}");
            ExecutionResultAssert.assertThat(executionResult).isDataEqualTo(
                    "{cows=[{cow={id=1, name=Friggy}, stable={name=Freital}}, {cow={id=2, name=Birgit}, stable={name=Freital}}]}");
        }

        @Test
        public void testLoadCowWithStableAtDefault() {
            GraphQL graphQL = initGraphQl(buildCowRepository());

            /* execute query for hello (without parameters) */
            ExecutionResult executionResult = graphQL.execute("{cows{cow{id, name}, stable{name}}}");
            ExecutionResultAssert.assertThat(executionResult).isDataEqualTo(
                    "{cows=[{cow={id=1, name=Friggy}, stable={name=Dresden}}, {cow={id=2, name=Birgit}, stable={name=Hamburg}}]}");
        }
    }


    /**
     * The {@link Stable} become injected into the returned {@link Cow} result.
     * Attention: the Java {@link Cow} type itself is not enhanced, instead the "injection" is done only in the 
     * GraphQL Type: "Cow"
     * <pre> 
     * type Cow {
     *           id:ID!,
     *           name:String,
     *           stable(date:String="NOW"):Stable!
     *          }
     * </pre>
     * with help
     * 
     * of an {@link DataFetcher} that is registered by:
     * {@code type("Cow", builder -> builder.dataFetcher("stable", new StableResultFetcher()))}
     */
    @Nested
    public static class InternalInjectedFunctionTest {
        // @formatter:off
        private static final String SCHEMA =
             "type Query{cows: [Cow!]!} \n"
           + "type Cow {id:ID!, name:String, stable(date:String=\"NOW\"):Stable!} \n"
           + "type Stable {id:ID!, name:String} \n";
        // @formatter:on

        /**
         * Fetch a cow and its current residence.
         *
         * This Data Fetcher return for each cow, the cow itselfe and the stable where it resists.
         * The point in time for the residence is take from the argument "date" of stable function.
         *
         * The important point is, that the cow-stable information is only loaded when the "stable"
         * is in the SelectionSet.
         */
        public static class CowDataFetcher implements DataFetcher<DataFetcherResult<List<Cow>>> {

            private final CowRepository cowRepository;

            public CowDataFetcher(final CowRepository cowRepository) {
                this.cowRepository = cowRepository;
            }

            @Override
            public DataFetcherResult<List<Cow>> get(final DataFetchingEnvironment environment) throws Exception {

                boolean requiresResidence = environment.getSelectionSet().contains("stable/**");

                if (requiresResidence) {
                    SelectedField field = environment.getSelectionSet().getField("stable");
                    assert (field != null);
                    Optional<String> dateArgument = Optional.ofNullable((String) field.getArguments().get("date"));
                    LocalDate date = dateArgument.map(Util::parseDateArgument).orElse(NOW);

                    Map<Cow, Optional<Stable>> stableByCow = this.cowRepository.findAllAndStableAtDate(date)
                            .collect(Collectors.toMap(Pair::getLeft, Pair::getRight));
                    Builder<List<Cow>> result = DataFetcherResult.<List<Cow>> newResult();
                    result.data(stableByCow.keySet().stream().sorted(Cow.COW_BY_ID_COMPARATOR)
                            .collect(Collectors.toList()));
                    result.localContext(Map.of("stableByCow", stableByCow));

                    return result.build();
                } else {
                    return DataFetcherResult.<List<Cow>> newResult().data(this.cowRepository.findAllAsStream()
                            .sorted(Cow.COW_BY_ID_COMPARATOR).collect(Collectors.toList())).build();
                }
            }
        }

        public static class StableResultFetcher implements DataFetcher<Stable> {

            @Override
            public Stable get(final DataFetchingEnvironment environment) throws Exception {
                Object localContext = environment.getLocalContext();
                if (localContext == null) {
                    throw new IllegalArgumentException("LocalContext is null");
                }
                if (!(localContext instanceof Map)) {
                    throw new IllegalArgumentException(
                            "LocalContext is not a Map (instead it is of type " + localContext.getClass() + ")");
                }

                @SuppressWarnings({ "unchecked", "rawtypes" })
                Map<Cow, Optional<Stable>> stableByCow = (Map<Cow, Optional<Stable>>) ((Map) localContext)
                        .get("stableByCow");

                Cow cow = (Cow) environment.getSource();
                return stableByCow.get(cow).orElse(null);
            }
        }

        private GraphQL initGraphQl(final CowRepository cowRepository) {
            SchemaParser schemaParser = new SchemaParser();
            TypeDefinitionRegistry typeDefinitionRegistry = schemaParser.parse(SCHEMA);

            RuntimeWiring runtimeWiring = RuntimeWiring.newRuntimeWiring()
                    .type("Query", builder -> builder.dataFetcher("cows", new CowDataFetcher(cowRepository)))
                    .type("Cow", builder -> builder.dataFetcher("stable", new StableResultFetcher())).build();

            SchemaGenerator schemaGenerator = new SchemaGenerator();
            GraphQLSchema graphQLSchema = schemaGenerator.makeExecutableSchema(typeDefinitionRegistry, runtimeWiring);

            return GraphQL.newGraphQL(graphQLSchema).build();
        }

        @Test
        public void testLoadCowOnly() {
            GraphQL graphQL = initGraphQl(buildCowRepository());

            /* execute query for hello (without parameters) */
            ExecutionResult executionResult = graphQL.execute("{cows{id, name}}");
            ExecutionResultAssert.assertThat(executionResult)
                    .isDataEqualTo("{cows=[{id=1, name=Friggy}, {id=2, name=Birgit}]}");
        }

        @Test
        public void testLoadCowWithStableNow() {
            GraphQL graphQL = initGraphQl(buildCowRepository());

            /* execute query for hello (without parameters) */
            ExecutionResult executionResult = graphQL.execute("{cows{id, name, stable(date:\"NOW\"){name}}}");
            ExecutionResultAssert.assertThat(executionResult).isDataEqualTo(
                    "{cows=[{id=1, name=Friggy, stable={name=Dresden}}, {id=2, name=Birgit, stable={name=Hamburg}}]}");
        }

        @Test
        public void testLoadCowWithStableYesterDay() {
            GraphQL graphQL = initGraphQl(buildCowRepository());

            /* execute query for hello (without parameters) */
            ExecutionResult executionResult = graphQL.execute("{cows{id, name, stable(date:\"YESTERDAY\"){name}}}");
            ExecutionResultAssert.assertThat(executionResult).isDataEqualTo(
                    "{cows=[{id=1, name=Friggy, stable={name=Freital}}, {id=2, name=Birgit, stable={name=Freital}}]}");
        }

        @Test
        public void testLoadCowWithStableAtDefault() {
            GraphQL graphQL = initGraphQl(buildCowRepository());

            /* execute query for hello (without parameters) */
            ExecutionResult executionResult = graphQL.execute("{cows{id, name, stable{name}}}");
            ExecutionResultAssert.assertThat(executionResult).isDataEqualTo(
                    "{cows=[{id=1, name=Friggy, stable={name=Dresden}}, {id=2, name=Birgit, stable={name=Hamburg}}]}");
        }
    }
}
