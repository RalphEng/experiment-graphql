package de.humanfork.experiment.graphql;

import org.junit.jupiter.api.Test;

import de.humanfork.experiment.graphql.asserts.ExecutionResultAssert;
import graphql.ExecutionResult;
import graphql.GraphQL;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.GraphQLSchema;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.SchemaGenerator;
import graphql.schema.idl.SchemaParser;
import graphql.schema.idl.TypeDefinitionRegistry;

/**
 * Test how to insert data from a data fetcher into a type.
 *
 * The trick is, that the data fetcher is assigned to the type but not to "Query":
 * {@code
 * .type("Cow", builder -> builder.dataFetcher("stable", new StableDataFetcher()))
 * }
 *
 *
 * @author engelmann
 */
public class DataFetchInsertTest {

 // @formatter:off
    private static final String SCHEMA =
             "type Query{cows: Cow!} \n"
           + "type Cow {id:ID!, name:String!, stable:Stable} \n"
           + "type Stable {id:ID!, name:String!} \n";
    // @formatter:on

    /** Straight forward domain object. */
    public static class Cow {

        @SuppressWarnings("unused")
        private final int id;

        @SuppressWarnings("unused")
        private final String name;

        public Cow(final int id, final String name) {
            this.id = id;
            this.name = name;
        }
    }

    /** Straight forward domain object. */
    public static class Stable {

        @SuppressWarnings("unused")
        private final int id;

        @SuppressWarnings("unused")
        private final String name;

        public Stable(final int id, final String name) {
            this.id = id;
            this.name = name;
        }

    }

    public static class CowDataFetcher implements DataFetcher<Cow> {

        private final Cow friggy = new Cow(1, "Friggy");

        @Override
        public Cow get(final DataFetchingEnvironment environment) throws Exception {
            return this.friggy;
        }
    }

    public static class StableDataFetcher implements DataFetcher<Stable> {

        private final Stable dresden = new Stable(1, "Dresden");

        @Override
        public Stable get(final DataFetchingEnvironment environment) throws Exception {
            return this.dresden;
        }
    }

    private GraphQL initGraphQl() {
        SchemaParser schemaParser = new SchemaParser();
        TypeDefinitionRegistry typeDefinitionRegistry = schemaParser.parse(SCHEMA);

        RuntimeWiring runtimeWiring = RuntimeWiring.newRuntimeWiring()
                .type("Query", builder -> builder.dataFetcher("cows", new CowDataFetcher()))
                .type("Cow", builder -> builder.dataFetcher("stable", new StableDataFetcher())).build();

        SchemaGenerator schemaGenerator = new SchemaGenerator();
        GraphQLSchema graphQLSchema = schemaGenerator.makeExecutableSchema(typeDefinitionRegistry, runtimeWiring);

        return GraphQL.newGraphQL(graphQLSchema).build();
    }

    @Test
    public void testLoadCowOnly() {
        GraphQL graphQL = initGraphQl();

        /* execute query for hello (without parameters) */
        ExecutionResult executionResult = graphQL.execute("{cows{id, name, stable{id, name}}}");
        ExecutionResultAssert.assertThat(executionResult)
                .isDataEqualTo("{cows={id=1, name=Friggy, stable={id=1, name=Dresden}}}");
    }
}
