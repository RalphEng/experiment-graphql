package de.humanfork.experiment.graphql.asserts;

import java.util.function.Consumer;

import org.assertj.core.api.AbstractObjectAssert;

import graphql.schema.GraphQLFieldDefinition;
import graphql.schema.GraphQLObjectType;

public class GraphQLObjectTypeAssert extends AbstractObjectAssert<GraphQLObjectTypeAssert, GraphQLObjectType> {

    public GraphQLObjectTypeAssert(GraphQLObjectType actual) {
        super(actual, GraphQLObjectTypeAssert.class);
    }

    /**
     * Fluent entry point, can be used with static import.
     * 
     * @param actual the found execution GraphQLObjectType
     * @return the matcher
     */
    public static GraphQLObjectTypeAssert assertThat(final GraphQLObjectType actual) {
        return new GraphQLObjectTypeAssert(actual);
    }
        
    
    public GraphQLObjectTypeAssert hasFieldDefinition(String fieldName,  Consumer<GraphQLFieldDefinitionAssert> requirements) {                
        GraphQLFieldDefinition fieldDefinition = actual.getFieldDefinition(fieldName);
        
        if (actual == null) {
            failWithMessage("Expected field definition of name <%s> to be present, but it does not exist", fieldDefinition);
        }
        
        requirements.accept(GraphQLFieldDefinitionAssert.assertThat(fieldDefinition));
        
        return this;
    }
    
    public GraphQLObjectTypeAssert hasFieldDefinition(String fieldName) {                
        hasFieldDefinition(fieldName, (a) -> {});
        return this;
     }
}
