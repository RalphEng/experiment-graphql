package de.humanfork.experiment.graphql.asserts;

import java.util.List;

import org.assertj.core.api.AbstractObjectAssert;

import graphql.schema.GraphQLArgument;
import graphql.schema.GraphQLFieldDefinition;

public class GraphQLFieldDefinitionAssert  extends AbstractObjectAssert<GraphQLFieldDefinitionAssert, GraphQLFieldDefinition> {

    public GraphQLFieldDefinitionAssert(GraphQLFieldDefinition actual) {
        super(actual, GraphQLFieldDefinitionAssert.class);
    }

    /**
     * Fluent entry point, can be used with static import.
     * 
     * @param actual the found execution GraphQLFieldDefinition
     * @return the matcher
     */
    public static GraphQLFieldDefinitionAssert assertThat(final GraphQLFieldDefinition actual) {
        return new GraphQLFieldDefinitionAssert(actual);
    }
    
    public GraphQLFieldDefinitionAssert isOfType(String expectedType) {
        isNotNull();
        
        String actualType = actual.getType().toString();
        
        if (!actualType.equals(expectedType)) {
            failWithMessage("Expected field definition of name <%s> to be of type <%s>, but was <%s>", actual.getName(), expectedType, actualType);
        }
        return this;
    }
    
    public GraphQLFieldDefinitionAssert hasNoArgurments() {
        isNotNull();
        
        
        List<GraphQLArgument> actualArguments = actual.getArguments();
                
        if (!actualArguments.isEmpty()) {
            failWithMessage("Expected field definition of name <%s> to have no arguments, but it has arguments: <%s>", actual.getName(), actualArguments);
        }
        return this;
    }

}
