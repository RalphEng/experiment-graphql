package de.humanfork.experiment.graphql.asserts;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Map;
import java.util.stream.Collectors;

import org.assertj.core.api.AbstractObjectAssert;

import graphql.ExceptionWhileDataFetching;
import graphql.ExecutionResult;
import graphql.GraphQLError;

public class ExecutionResultAssert extends AbstractObjectAssert<ExecutionResultAssert, ExecutionResult> {

    public ExecutionResultAssert(ExecutionResult actual) {
        super(actual, ExecutionResultAssert.class);
    }

    /**
     * Fluent entry point, can be used with static import.
     * 
     * @param actual the found execution result
     * @return the matcher
     */
    public static ExecutionResultAssert assertThat(final ExecutionResult actual) {
        return new ExecutionResultAssert(actual);
    }

    public ExecutionResultAssert hasNoErrors() {
        isNotNull();

        if (!actual.getErrors().isEmpty()) {
            failWithMessage("Expected no errors,  but was <%s>",
                    actual.getErrors().stream().map(this::errorToString).collect(Collectors.joining(", ", "[", "]")));
        }

        return this;
    }

    public String errorToString(GraphQLError error) {
        StringBuilder s = new StringBuilder();
        s.append(error.toString());
        if (error instanceof ExceptionWhileDataFetching) {
            ExceptionWhileDataFetching exceptionWhileDataFetching = (ExceptionWhileDataFetching) error;
            Throwable exception = exceptionWhileDataFetching.getException();
            try (StringWriter sw = new StringWriter(); PrintWriter pw = new PrintWriter(sw)) {
                exception.printStackTrace(pw);
                s.append("trace:\n" + sw.toString());
            } catch (IOException e) {
                throw new RuntimeException("error while printing exception", e);
            }
        }

        return s.toString();
    }

    public ExecutionResultAssert hasError(Class<? extends GraphQLError> errorClass) {
        isNotNull();

        if (actual.getErrors().isEmpty()) {
            failWithMessage("Expected error <%s>, but no error was found", errorClass.getName());
        }
        if (actual.getErrors().size() > 1) {
            failWithMessage("Expected single error <%s>, but found %s errors: <%s>",
                    errorClass.getName(),
                    actual.getErrors().size(),
                    actual.getErrors());
        }
        if (!actual.getErrors().get(0).getClass().equals(errorClass)) {
            failWithMessage("Expected error <%s>, but found <%s>",
                    errorClass.getName(),
                    actual.getErrors().get(0).getClass());
        }

        return this;
    }

    public ExecutionResultAssert isDataPresent() {
        isNotNull();

        if (!actual.isDataPresent()) {
            failWithMessage("Expected data to be present, but was not");
        }

        return this;
    }

    public ExecutionResultAssert isDataEqualTo(final String expected) {
        isNotNull();
        hasNoErrors();
        isDataPresent();

        if (!actual.getData().toString().equals(expected)) {
            failWithMessage("Expected data to be equals to <%s>, but was <%s>", expected, actual.getData().toString());
        }

        return this;
    }

    public ExecutionResultAssert isDataEqualTo(final Map<?, ?> expected) {
        isNotNull();
        hasNoErrors();
        isDataPresent();

        if (!actual.getData().equals(expected)) {
            failWithMessage("Expected data to equals to <%s> (%s), but was <%s>",
                    actual.getData(),
                    actual.getData().getClass(),
                    expected);
        }

        return this;
    }

}
