package de.humanfork.experiment.graphql;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.lang.reflect.Field;

import org.junit.jupiter.api.Test;

import de.humanfork.experiment.graphql.asserts.ExecutionResultAssert;
import graphql.GraphQL;
import graphql.GraphQLException;
import graphql.schema.DataFetcher;
import graphql.schema.GraphQLSchema;
import graphql.schema.PropertyDataFetcher;
import graphql.schema.PropertyDataFetcherHelper;
import graphql.schema.idl.FieldWiringEnvironment;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.SchemaGenerator;
import graphql.schema.idl.SchemaParser;
import graphql.schema.idl.TypeDefinitionRegistry;
import graphql.schema.idl.WiringFactory;

/**
 * Show how to disable a field (in the response) based on some logic (secruity decision).
 * 
 * <p>
 * This test use a custom {@link PropertyDataFetcher} {@code SecuredPropertyDataFetcher} that become registered as
 * default data fetcher via "custom" WiringFactory.
 * </p>
 * <p>
 * {@code SecuredPropertyDataFetcher} is a data fetcher that replace the content of all fields that are:
 * annotateted {@code FieldLevelSecurity} by and where the user have no access to (decision is made by {@link MockSecuritySystem})
 * with {@code null}.
 * </p>
 */
public class FieldLevelSecurityTest {

    private static final class MockSecuritySystem {

        private boolean grantAccess;

        public MockSecuritySystem(final boolean grantAccess) {
            this.grantAccess = grantAccess;
        }

        public static MockSecuritySystem grantAll() {
            return new MockSecuritySystem(true);
        }

        public static MockSecuritySystem denySecured() {
            return new MockSecuritySystem(false);
        }

        public boolean hasReadAccessOnField(final Object object, final Field field) {
            return this.grantAccess;
        }
    }

    @Retention(RUNTIME)
    @Target(FIELD)
    public @interface FieldLevelSecurity {

    }

    public static class SecuredPropertyDataFetcher<T> extends PropertyDataFetcher<T> {

        private final MockSecuritySystem mockSecuritySystem;

        public SecuredPropertyDataFetcher(final String propertyName, final MockSecuritySystem mockSecuritySystem) {
            super(propertyName);
            this.mockSecuritySystem = mockSecuritySystem;
        }

        @Override
        public T get(final graphql.schema.DataFetchingEnvironment environment) {
            Object source = environment.getSource();
            if (source == null) {
                return null;
            }

            /*
             * just a prove of concept
             * this implementation does not handle method invocations.
             * see:  PropertyDataFetcherHelper.getPropertyValue(propertyName, source, environment.getFieldType(), environment)
             * for full set of needed features 
             */
            
            Field field = getField(source, getPropertyName());
            boolean isSecured = field.getAnnotation(FieldLevelSecurity.class) != null;
            if (isSecured && !this.mockSecuritySystem.hasReadAccessOnField(source, field)) {
                return null;
            } else {
                return (T) PropertyDataFetcherHelper
                        .getPropertyValue(getPropertyName(), source, environment.getFieldType(), environment);
            }
        }

        private static Field getField(final Object object, final String propertyName) {
            Class<?> clazz = object.getClass();
            try {
                return clazz.getField(propertyName);
            } catch (NoSuchFieldException ignored1) {
                try {
                    Field field = clazz.getDeclaredField(propertyName);
                    field.setAccessible(true);
                    return field;
                } catch (NoSuchFieldException | SecurityException e) {
                    throw new GraphQLException(e);
                }
            }
        }
    }

    public static class User {
        private final int id;

        private final String name;

        @FieldLevelSecurity
        private final String secret;

        private final Account account;

        public User(final int id, final String name, final String secret, final Account account) {
            this.id = id;
            this.name = name;
            this.secret = secret;
            this.account = account;
        }

        @Override
        public String toString() {
            return "User [id=" + id + ", name=" + name + ", secret=" + secret + ", account=" + account + "]";
        }

    }

    public static class Account {
        @FieldLevelSecurity
        private final String login;

        public Account(final String login) {
            this.login = login;
        }
    }

    public GraphQL buildGraphQlApi(final MockSecuritySystem mockSecuritySystem) {
        // @formatter:off
        String schema =
                 "type Query{getUserById(id:Int!): User!} \n"
               + "type User {id:ID!, name:String!, secret:String, account:Account}"
               + "type Account {login:String}";
        // @formatter:on

        SchemaParser schemaParser = new SchemaParser();

        TypeDefinitionRegistry typeDefinitionRegistry = schemaParser.parse(schema);
        RuntimeWiring runtimeWiring = RuntimeWiring.newRuntimeWiring()
                .type("Query", builder -> builder.dataFetcher("getUserById", environment -> {
                    Integer id = environment.getArgument("id");
                    return new User(id, "name" + id, "secret" + id, new Account("login" + id));
                }))
               
                /* register custom Property-Data-Fetcher */
                .wiringFactory(new WiringFactory() {
                    @Override
                    @SuppressWarnings("rawtypes")
                    public DataFetcher getDefaultDataFetcher(final FieldWiringEnvironment environment) {
                        return new SecuredPropertyDataFetcher<>(environment.getFieldDefinition().getName(),
                                mockSecuritySystem);
                    };
                })
                .build();

        SchemaGenerator schemaGenerator = new SchemaGenerator();
        GraphQLSchema graphQLSchema = schemaGenerator.makeExecutableSchema(typeDefinitionRegistry, runtimeWiring);

        return GraphQL.newGraphQL(graphQLSchema).build();
    }

    /** grant access to: user.secret (must be "secret1") */
    @Test
    public void testTopLevelField_grant() {
        ExecutionResultAssert
                .assertThat(
                        buildGraphQlApi(MockSecuritySystem.grantAll()).execute("{getUserById(id:1){id, name, secret}}"))
                .isDataEqualTo("{getUserById={id=1, name=name1, secret=secret1}}");
    }

    /** deny access to: user.secret (must be null) */
    @Test
    public void testTopLevelField_deny() {
        ExecutionResultAssert
                .assertThat(
                        buildGraphQlApi(MockSecuritySystem.denySecured())
                                .execute("{getUserById(id:1){id, name, secret}}"))
                .isDataEqualTo("{getUserById={id=1, name=name1, secret=null}}");
    }

    /** grant access to: user.account.login (must be "login1") */
    @Test
    public void testSubfield_grant() {
        ExecutionResultAssert
                .assertThat(buildGraphQlApi(MockSecuritySystem.grantAll())
                        .execute("{getUserById(id:1){id, account {login}}}"))
                .isDataEqualTo("{getUserById={id=1, account={login=login1}}}");
    }

    /** deny access to: user.account.login (must be null) */
    @Test
    public void testSubfield_deny() {
        ExecutionResultAssert
                .assertThat(buildGraphQlApi(MockSecuritySystem.denySecured())
                        .execute("{getUserById(id:1){id, account {login}}}"))
                .isDataEqualTo("{getUserById={id=1, account={login=null}}}");
    }
}
