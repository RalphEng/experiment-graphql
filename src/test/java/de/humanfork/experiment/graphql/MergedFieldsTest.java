package de.humanfork.experiment.graphql;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Test;

import de.humanfork.experiment.graphql.asserts.ExecutionResultAssert;
import de.humanfork.experiment.graphql.cowdomain.Cow;
import graphql.GraphQL;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.GraphQLSchema;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.SchemaGenerator;
import graphql.schema.idl.SchemaParser;
import graphql.schema.idl.TypeDefinitionRegistry;

/**
 * https://www.graphql-java.com/blog/deep-dive-merged-fields/
 */
public class MergedFieldsTest {

    public GraphQL buildGraphQlApi() {
        String schema = "type Query { cow(id:Int!): Cow }  \n"
                + "type Cow {id:ID!, name:String}         \n";

        SchemaParser schemaParser = new SchemaParser();
        TypeDefinitionRegistry typeDefinitionRegistry = schemaParser.parse(schema);

        RuntimeWiring runtimeWiring = RuntimeWiring.newRuntimeWiring()
                .type("Query", builder -> builder.dataFetcher("cow", new CowDataFetcher()))
                .build();

        SchemaGenerator schemaGenerator = new SchemaGenerator();
        GraphQLSchema graphQLSchema = schemaGenerator.makeExecutableSchema(typeDefinitionRegistry, runtimeWiring);

        return GraphQL.newGraphQL(graphQLSchema).build();
    }

    public static class CowDataFetcher implements DataFetcher<Optional<Cow>> {

        private List<Cow> cows = Arrays.asList(new Cow(1, "Friggy"),
                new Cow(2, "Birgit"));

        @Override
        public Optional<Cow> get(final DataFetchingEnvironment environment) throws Exception {
            int id = environment.getArgument("id");
            return this.cows.stream().filter(cow -> cow.getId() == id).findAny();
        }
    }

    @Test
    public void testMergeSameCowFromTwoQueries() {
        ExecutionResultAssert
                .assertThat(buildGraphQlApi().execute("query mergeAttributes {cow(id:1){id}, cow(id:1){name}}"))
                .isDataEqualTo("{cow={id=1, name=Friggy}}");
    }

    /**
     * Motivation for merged fields:
     * 
     * Fragments are designed to be written by different parties (for example
     * different components in a UI) which should not know anything about each
     * other. Requiring that every field can only be declared once would make this
     * objective unfeasible.
     *
     * But by allowing fields merging, as long as the fields are the same, allows
     * fragments to be authored in an independent way from each other.
     */
    @Test
    public void testMergeFromFragments() {
        // @formatter:off
        ExecutionResultAssert.assertThat(buildGraphQlApi().execute(
                "query mergedQuery {              \n"
                        + "   ...myFragment1               \n"
                        + "   ...myFragment2               \n"
                        + "}                               \n"
                        + "fragment myFragment1 on Query { \n"
                        + "   cow(id:1) {id}               \n"
                        + "}                               \n"
                        + "fragment myFragment2 on Query { \n"
                        + "   cow(id:1) {name}             \n"
                        + "}                               \n"))
                .isDataEqualTo("{cow={id=1, name=Friggy}}");
        // @formatter:on
    }
}
