package de.humanfork.experiment.graphql;

import java.util.Optional;

import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import de.humanfork.experiment.graphql.asserts.ExecutionResultAssert;
import graphql.GraphQL;
import graphql.schema.GraphQLSchema;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.SchemaGenerator;
import graphql.schema.idl.SchemaParser;
import graphql.schema.idl.TypeDefinitionRegistry;

public class EnumTest {

    static enum Planet {
        VENUS,
        EARTH,
        MARS;

        /**
         * Return the next outer planet.
         */
        public Optional<Planet> getNextOuterPlanet() {
            boolean isLast = this.ordinal() == Planet.values().length - 1;
            if (isLast) {
                return Optional.empty();
            } else {
                return Optional.of(Planet.values()[this.ordinal() + 1]);
            }
        }
    }

    /**
     * environment argruments are are expressed by graphql-java as String.
     * 
     * Because we use Jackson to map complex input argruments, we use it also for
     * String to Enum mapping.
     */
    private final ObjectMapper mapper = new ObjectMapper();

    public GraphQL buildGraphQlApi() {
        // @formatter:off
        String schema = "type Query {                      \n"
                + "  currentPlanet: Planet!                \n"
                + "  nextPlanet(planet: Planet!): Planet    \n"
                + "}                                       \n"
                + "enum Planet {VENUS, EARTH, MARS}";
        // @formatter:on

        SchemaParser schemaParser = new SchemaParser();
        TypeDefinitionRegistry typeDefinitionRegistry = schemaParser.parse(schema);

        RuntimeWiring runtimeWiring = RuntimeWiring.newRuntimeWiring()
                .type("Query",
                        builder -> builder.dataFetcher("currentPlanet", environment -> Planet.EARTH)
                                .dataFetcher("nextPlanet", environment -> {
                                    Planet planet = mapper.convertValue(environment.getArgument("planet"), Planet.class);
                                    
                                    return planet.getNextOuterPlanet();
                                }))
                .build();

        SchemaGenerator schemaGenerator = new SchemaGenerator();
        GraphQLSchema graphQLSchema = schemaGenerator.makeExecutableSchema(typeDefinitionRegistry, runtimeWiring);

        return GraphQL.newGraphQL(graphQLSchema).build();
    }

    @Test
    public void testEnumOutputType() {
        ExecutionResultAssert.assertThat(buildGraphQlApi().execute("query {currentPlanet}"))
                .isDataEqualTo("{currentPlanet=EARTH}");
    }

    @Test
    public void testEnumInputType() {
        ExecutionResultAssert.assertThat(buildGraphQlApi().execute("query {nextPlanet(planet:EARTH)}"))
                .isDataEqualTo("{nextPlanet=MARS}");
    }
}
