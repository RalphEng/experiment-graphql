package de.humanfork.experiment.graphql.util;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import graphql.language.Selection;
import graphql.schema.DataFetchingEnvironment;

public class Debug {

    @SuppressWarnings("rawtypes")
    private List<Selection> rawSelection;

    @SuppressWarnings("rawtypes")
    public Debug(List<Selection> rawSelection) {
        this.rawSelection = rawSelection;
    }

    public String getRawSelections() {
        return this.rawSelection != null ? this.rawSelection.toString() : null;
    }

    /**
     * Return a string that contains the given attribute value for all selections (from rawSelections).
     * If there is no attribute defined, then the result contains the full string representation for all 
     * {@link Selection}s.
     * 
     * Current supported attributes are: "name".
     *   
     * @param environment graphql environment, containing the a value called "attribute"
     * @return the selections (attribute)
     */
    public String getSelections(final DataFetchingEnvironment environment) {
        Optional<String> attribute = Optional.of(environment.getArgument("attribute"));

        if (attribute.isEmpty()) {
            return this.rawSelection != null ? this.rawSelection.toString() : null;
        } else if (attribute.get().equalsIgnoreCase("name")) {
            return this.rawSelection != null ? this.rawSelection.stream()
                    .map(s -> ((graphql.language.Field) s).getName()).collect(Collectors.joining(", ", "[", "]"))
                    : null;
        } else {
            throw new IllegalArgumentException("attribute `" + attribute.get() + "` is not supported");
        }
    }

}