package de.humanfork.experiment.graphql;

import java.util.List;

import org.junit.jupiter.api.Test;

import de.humanfork.experiment.graphql.asserts.ExecutionResultAssert;
import de.humanfork.experiment.graphql.util.Debug;
import graphql.ExecutionResult;
import graphql.GraphQL;
import graphql.language.Selection;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.GraphQLSchema;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.SchemaGenerator;
import graphql.schema.idl.SchemaParser;
import graphql.schema.idl.TypeDefinitionRegistry;

/**
 * Test how the GraphGL query-selection-set can be accessed within the {@link DataFetcher}.
 *  
 * @author engelmann
 */
public class DataFetchAccessQueriedSelectionSetTest {

    public static class User {
        @SuppressWarnings("unused")
        private final int id;

        @SuppressWarnings("unused")
        private final String firstName;

        @SuppressWarnings("unused")
        private final String lastName;

        /**
         * Debug is used to pass some debug/logging information, back to the "client" in order to evaluate them in the
         * test.
         */
        @SuppressWarnings("unused")
        private final Debug debug;

        public User(final int id, final String firstName, final String lastName, final Debug debug) {
            this.id = id;
            this.firstName = firstName;
            this.lastName = lastName;
            this.debug = debug;
        }
    }

    public static class CurrentUserDataFetcher implements DataFetcher<User> {

        @Override
        public User get(final DataFetchingEnvironment environment) throws Exception {
            
            /** Obtain the query selection set. */
            @SuppressWarnings("rawtypes")
            List<Selection> querySelectionSet = environment.getField().getSelectionSet().getSelections();
           
            Debug debug = new Debug(querySelectionSet);
            
            return new User(1, "Ralph", "Engelmann", debug);
        }
    }

    /**
     * Example: how to register and invoke a method with one parameter
     * (HelloWorldCountDataFetcher)
     */
    @Test
    public void testTypeExample() {
        // @formatter:off
         String schema = 
                  "type Query{currentUser: User!} \n"
                + "type User {id:ID!, firstName:String!, lastName:String!, debug:Debug!} \n" 
                + "type Debug { rawSelections:String, selections(attribute:String):String," + "} ";
        // @formatter:on

        SchemaParser schemaParser = new SchemaParser();
        TypeDefinitionRegistry typeDefinitionRegistry = schemaParser.parse(schema);

        RuntimeWiring runtimeWiring = RuntimeWiring.newRuntimeWiring()
                .type("Query", builder -> builder.dataFetcher("currentUser", new CurrentUserDataFetcher())).build();

        SchemaGenerator schemaGenerator = new SchemaGenerator();
        GraphQLSchema graphQLSchema = schemaGenerator.makeExecutableSchema(typeDefinitionRegistry, runtimeWiring);

        /* execute query for hello (without parameters) */
        GraphQL build = GraphQL.newGraphQL(graphQLSchema).build();
        ExecutionResult executionResult = build
                .execute("{currentUser{id, firstName, debug{selections(attribute:\"Name\")}}}");

        /*
         * assert that the result contains only id, firstname and debug.selections information, and
         * that debug.selections is  [id, firstName, debug]
         */
        ExecutionResultAssert.assertThat(executionResult)
                .isDataEqualTo("{currentUser={id=1, firstName=Ralph, debug={selections=[id, firstName, debug]}}}");

    }

}
