package de.humanfork.experiment.graphql;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.Map;

import org.junit.jupiter.api.Test;

import de.humanfork.experiment.graphql.asserts.ExecutionResultAssert;
import graphql.ExecutionResult;
import graphql.GraphQL;
import graphql.Scalars;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.GraphQLArgument;
import graphql.schema.GraphQLFieldDefinition;
import graphql.schema.GraphQLList;
import graphql.schema.GraphQLNonNull;
import graphql.schema.GraphQLSchema;
import graphql.schema.GraphQLType;
import graphql.schema.StaticDataFetcher;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.SchemaGenerator;
import graphql.schema.idl.SchemaParser;
import graphql.schema.idl.TypeDefinitionRegistry;

/**
 *
 * @author Ralph Engelmann
 */
public class HelloWorldTest {


    @Test
    public void testSimplestExampleEver() {
        String schema = "type Query{hello: String}";

        SchemaParser schemaParser = new SchemaParser();
        TypeDefinitionRegistry typeDefinitionRegistry = schemaParser.parse(schema);

        RuntimeWiring runtimeWiring = RuntimeWiring.newRuntimeWiring()
                .type("Query", builder -> builder.dataFetcher("hello", new StaticDataFetcher("world"))).build();

        SchemaGenerator schemaGenerator = new SchemaGenerator();
        GraphQLSchema graphQLSchema = schemaGenerator.makeExecutableSchema(typeDefinitionRegistry, runtimeWiring);

        /* check the schema */
        assertThat(graphQLSchema.getQueryType()).isNotNull();
        assertThat(graphQLSchema.getQueryType().getFieldDefinitions()).extracting(GraphQLFieldDefinition::getName)
                .containsExactly("hello");
        assertThat(graphQLSchema.getQueryType().getFieldDefinition("hello").getArguments()).isEmpty();
        assertThat(graphQLSchema.getQueryType().getFieldDefinition("hello").getType()).isEqualTo(Scalars.GraphQLString);

        /* execute query for hello (without parameters) */
        GraphQL build = GraphQL.newGraphQL(graphQLSchema).build();
        ExecutionResult executionResult = build.execute("{hello}");

        /* assert that the result is "world" */
        ExecutionResultAssert.assertThat(executionResult).isDataEqualTo("{hello=world}");
        ExecutionResultAssert.assertThat(executionResult).isDataEqualTo(Map.of("hello", "world"));
    }

    public static class HelloWorldCountDataFetcher implements DataFetcher<String> {

        @Override
        public String get(final DataFetchingEnvironment environment) throws Exception {
            int count = environment.getArgument("count");
            return "world".repeat(count);
        }
    }

    /**
     * Example: how to register and invoke a method with one parameter
     * (HelloWorldCountDataFetcher)
     */
    @Test
    public void testArgumentExample() {
        String schema = "type Query{hello(count: Int): String}";

        SchemaParser schemaParser = new SchemaParser();
        TypeDefinitionRegistry typeDefinitionRegistry = schemaParser.parse(schema);

        RuntimeWiring runtimeWiring = RuntimeWiring.newRuntimeWiring()
                .type("Query", builder -> builder.dataFetcher("hello", new HelloWorldCountDataFetcher())).build();

        SchemaGenerator schemaGenerator = new SchemaGenerator();
        GraphQLSchema graphQLSchema = schemaGenerator.makeExecutableSchema(typeDefinitionRegistry, runtimeWiring);

        /* check the schema */
        assertThat(graphQLSchema.getQueryType()).isNotNull();
        assertThat(graphQLSchema.getQueryType().getFieldDefinitions()).extracting(GraphQLFieldDefinition::getName)
                .containsExactly("hello");
        assertThat(graphQLSchema.getQueryType().getFieldDefinition("hello").getArguments())
                .extracting(GraphQLArgument::getName).containsExactly("count");
        assertThat(graphQLSchema.getQueryType().getFieldDefinition("hello").getType()).isEqualTo(Scalars.GraphQLString);

        /* execute query for hello (without parameters) */
        GraphQL build = GraphQL.newGraphQL(graphQLSchema).build();
        ExecutionResult executionResult = build.execute("{hello(count:2)}");

        /* assert that the result is 2x "world" */
        ExecutionResultAssert.assertThat(executionResult).isDataEqualTo("{hello=worldworld}");
        ExecutionResultAssert.assertThat(executionResult).isDataEqualTo(Map.of("hello", "worldworld"));
    }

    public static class Info {
        public int id;

        public String text;

        public Info(final int id, final String text) {
            this.id = id;
            this.text = text;
        }

    }

    /**
     * Example: how to register and invoke a method with one parameter
     * (HelloWorldCountDataFetcher)
     */
    @Test
    public void testTypeExample() {
        String schema = "type Query{info: Info!} \n" + "type Info {id:ID, text:String} ";

        SchemaParser schemaParser = new SchemaParser();
        TypeDefinitionRegistry typeDefinitionRegistry = schemaParser.parse(schema);

        RuntimeWiring runtimeWiring = RuntimeWiring.newRuntimeWiring()
                .type("Query", builder -> builder.dataFetcher("info", new StaticDataFetcher(new Info(1, "hello"))))
                .build();

        SchemaGenerator schemaGenerator = new SchemaGenerator();
        GraphQLSchema graphQLSchema = schemaGenerator.makeExecutableSchema(typeDefinitionRegistry, runtimeWiring);

        /* check the schema */
        assertThat(graphQLSchema.getQueryType()).isNotNull();
        assertThat(graphQLSchema.getQueryType().getFieldDefinitions()).extracting(GraphQLFieldDefinition::getName)
                .containsExactly("info");

        GraphQLFieldDefinition infoFieldDefinition = graphQLSchema.getQueryType().getFieldDefinition("info");
        assertThat(infoFieldDefinition.getArguments()).isEmpty();
        assertThat(infoFieldDefinition.getType()).extracting(Object::toString).isEqualTo("Info!");
        assertThat(infoFieldDefinition.getType()).isInstanceOf(GraphQLNonNull.class);

//        GraphQLType infoType = ((GraphQLNonNull) (infoFieldDefinition.getType())).getWrappedType();
//        assertThat(infoType.getName()).isEqualTo("Info");
//        assertThat(infoType.getChildren()).extracting(GraphQLType::getName).containsExactlyInAnyOrder("id", "text");

        /* execute query for hello (without parameters) */
        GraphQL build = GraphQL.newGraphQL(graphQLSchema).build();
        ExecutionResult executionResult = build.execute("{info{id, text}}");

        /* assert that the result is an info object" */
        ExecutionResultAssert.assertThat(executionResult).isDataEqualTo("{info={id=1, text=hello}}");
        ExecutionResultAssert.assertThat(executionResult)
                .isDataEqualTo(Map.of("info", Map.of("id", "1", "text", "hello")));

    }

    /**
     * Example: how to register and invoke a method with one parameter
     * (HelloWorldCountDataFetcher)
     */
    @Test
    public void testTypeListExample() {
        String schema = "type Query{infos: [Info!]!} \n" + "type Info {id:ID, text:String} ";

        SchemaParser schemaParser = new SchemaParser();
        TypeDefinitionRegistry typeDefinitionRegistry = schemaParser.parse(schema);

        RuntimeWiring runtimeWiring = RuntimeWiring.newRuntimeWiring()
                .type("Query",
                        builder -> builder.dataFetcher("infos",
                                new StaticDataFetcher(Arrays.asList(new Info(1, "hello"), new Info(2, "world")))))
                .build();

        SchemaGenerator schemaGenerator = new SchemaGenerator();
        GraphQLSchema graphQLSchema = schemaGenerator.makeExecutableSchema(typeDefinitionRegistry, runtimeWiring);

        /* check the schema */
        assertThat(graphQLSchema.getQueryType()).isNotNull();
        assertThat(graphQLSchema.getQueryType().getFieldDefinitions()).extracting(GraphQLFieldDefinition::getName)
                .containsExactly("infos");

        GraphQLFieldDefinition infosFieldDefinition = graphQLSchema.getQueryType().getFieldDefinition("infos");
        assertThat(infosFieldDefinition.getArguments()).isEmpty();
        assertThat(infosFieldDefinition.getType()).extracting(Object::toString).isEqualTo("[Info!]!");        
//        assertThat(infosFieldDefinition.getType()).isInstanceOfSatisfying(GraphQLNonNull.class,
//                x1 -> assertThat(x1.getWrappedType()).isInstanceOfSatisfying(GraphQLList.class,
//                        x2 -> assertThat(x2.getWrappedType()).isInstanceOfSatisfying(GraphQLNonNull.class,
//                                x3 -> assertThat(x3.getWrappedType().getName()).isEqualTo("Info"))));

        assertThat(infosFieldDefinition.getArguments()).isEmpty();

        /* execute query for hello (without parameters) */
        GraphQL build = GraphQL.newGraphQL(graphQLSchema).build();
        ExecutionResult executionResult = build.execute("{infos{id, text}}");

        /* assert that the result is two info objects */
        ExecutionResultAssert.assertThat(executionResult)
                .isDataEqualTo("{infos=[{id=1, text=hello}, {id=2, text=world}]}");
        ExecutionResultAssert.assertThat(executionResult).isDataEqualTo(
                Map.of("infos", Arrays.asList(Map.of("id", "1", "text", "hello"), Map.of("id", "2", "text", "world"))));
    }
}
