package de.humanfork.experiment.graphql;

import java.util.List;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import de.humanfork.experiment.graphql.asserts.ExecutionResultAssert;
import graphql.ExecutionResult;
import graphql.GraphQL;
import graphql.schema.GraphQLSchema;
import graphql.schema.StaticDataFetcher;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.SchemaGenerator;
import graphql.schema.idl.SchemaParser;
import graphql.schema.idl.TypeDefinitionRegistry;

/**
 * DOES NOT WORK: Test how on could use Directives to add meta information to fields,
 * because Directives are not exposed in meta data.
 */
@Disabled("this test is disabled, because Directives are not exposed in meta data.")
public class DirectivesTest {

    // @formatter:off
        private static final String SCHEMA =    
             " directive @i18n(key : String!) on FIELD_DEFINITION | ENUM                \n"           
           + " directive @sort(                                                         \n"
           + "        name : String! = \"FIELD_NAME\"                                   \n"
           + "        direction : [Direction!]! = [ASC, DESC]                           \n"
           + " )  on FIELD_DEFINITION                                                   \n"
           + " directive @optionFilter(optionName : String!) on INPUT_FIELD_DEFINITION  \n"
           + " directive @enumFilter(enumName : String!) on INPUT_FIELD_DEFINITION      \n"
           + "                                                                          \n"
           + " enum Direction { ASC, DESC }                                             \n"
           + "                                                                          \n"
           + "                                                                          \n"
           + "                                                                          \n"
           + "                                                                          \n"
           + "type Query {                                                              \n"
           + "   getDemo(filter: DemoFilter): [Demo!]!                                  \n"
           + "   otherOptionsProvider(startsWith: String): [String!]!                   \n"
           + "}                                                                         \n"
           + "                                                                          \n"
           + "type Demo{                                                                \n"
           + "      '''@i18n(key : \"this is a i18n key\") @sort'''                     \n"
           + "      someString : String                                                 \n"
           + "      '''@i18n(key : \"this is a i18n key\") @sort(name : \"x\", direction : [ASC] )'''"
           + "      otherString : String                                                \n"
           + "      '''@i18n(key : \"c\")'''                                            \n"
           + "      classification : Classification                                     \n"
           + "}                                                                         \n"
           + "                                                                          \n"
           + "input DemoFilter{                                                         \n"
           + "    '''@optionFilter(optionName : \"otherOptions\")'''                    \n"
           + "    otherString : TextFilter                                              \n"
           + "    ''' @optionFilter(optionName : \"otherOptions\")'''                   \n"
           + "    classification : EnumFilter                                           \n"
           + "}                                                                         \n"
           + "                                                                          \n"
           + "input TextFilter {                                                        \n"
           + "   searchString : String                                                  \n"
           + "}                                                                         \n"           
           + "input EnumFilter {                                                        \n"
           + "   option : String                                                        \n"
           + "}                                                                         \n"
           + "                                                                          \n"
           + "enum Classification @i18n(key : \"de.humanfork.Classification\") {        \n"
           + "  A                                                                       \n"
           + "  B                                                                       \n"
           + "}                                                                         \n"
           + "                                                                          \n"
           + "                                                                          \n"
           + "                                                                          \n"
           ;
        // @formatter:on

    /** Helper class to combine cow and stable (at a secific point in time) for GraphQL query result. */
    public static class Demo {

        private String someString;

        private String otherString;

        public Demo(String someString, String otherString) {
            this.someString = someString;
            this.otherString = otherString;
        }
    }

    private GraphQL initGraphQl() {
        SchemaParser schemaParser = new SchemaParser();
        TypeDefinitionRegistry typeDefinitionRegistry = schemaParser.parse(SCHEMA);

        RuntimeWiring runtimeWiring = RuntimeWiring.newRuntimeWiring()
                .type("Query", builder -> builder.dataFetcher("getDemo", new StaticDataFetcher(List.of(new Demo("A", "B")))))
                .build();

        SchemaGenerator schemaGenerator = new SchemaGenerator();
        GraphQLSchema graphQLSchema = schemaGenerator.makeExecutableSchema(typeDefinitionRegistry, runtimeWiring);

        return GraphQL.newGraphQL(graphQLSchema).build();
    }
    
    @Test
    public void testLoad() {
        GraphQL graphQL = initGraphQl();

        /* execute query for hello (without parameters) */
        ExecutionResult executionResult = graphQL.execute("query {getDemo {someString, otherString}}");
        ExecutionResultAssert.assertThat(executionResult)
                .isDataEqualTo("{getDemo=[{someString=A, otherString=B}]}");
        
        
        
    }

}
