package de.humanfork.experiment.graphql;

import org.junit.jupiter.api.Test;

import de.humanfork.experiment.graphql.asserts.ExecutionResultAssert;
import graphql.ExecutionResult;
import graphql.GraphQL;
import graphql.execution.DataFetcherResult;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.GraphQLSchema;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.SchemaGenerator;
import graphql.schema.idl.SchemaParser;
import graphql.schema.idl.TypeDefinitionRegistry;

/**
 * Test how to pass information from one DataFetcher to an "child" DataFetcher via
 * {@link DataFetchingEnvironment#getLocalContext()} and {@link DataFetcherResult#getLocalContext()}.
 * 
 * Test how GraphQL behaves if the {@link  graphql.schema.DataFetcher} return a {@link DataFetcherResult}.
 * According to "https://www.graphql-java.com/blog/deep-dive-data-fetcher-results/":
 * <blockquote>
 *    In GraphQL Java you can use well known graphql.execution.DataFetcherResult to return three sets of values:
 *    <ul>
 *      <li>data - which will be used as the source on the next set of sub fields</li>
 *      <li>errors - allowing you to return data as well as errors</li>
 *      <li>localContext - which allows you to pass down field specific context</li>
 *    </ul>
 *    When the engine sees the graphql.execution.DataFetcherResult object, it automatically unpacks it and handles it
 *    three classes of data in specific ways.
 * </blockquote>
 * 
 * @author Ralph Engelmann
 */
public class DataFetchReturnTypeDataFetcherResult {

    // @formatter:off
    private static final String SCHEMA =
             "type Query  {rootQuery:Level1} \n"
           + "type Level1 {value:Level2} \n"
           + "type Level2 {value:String} \n";
    // @formatter:on

    public static class RootQueryDataFetcher implements DataFetcher<DataFetcherResult<Object>> {
        @Override
        public DataFetcherResult<Object> get(final DataFetchingEnvironment environment) throws Exception {
            return DataFetcherResult.newResult().data("rootData").localContext("localContextCreatedByRoot").build();
        }
    }

    public static class Level1DataFetcher implements DataFetcher<DataFetcherResult<Object>> {

        @Override
        public DataFetcherResult<Object> get(final DataFetchingEnvironment environment) throws Exception {
            
            System.out.println("level1: ");
            System.out.println("environment.getSource(): " + environment.getSource());
            System.out.println("environment.getLocalContext(): " + environment.getLocalContext());
            
            return DataFetcherResult.newResult().data(new Object()).build();
        }
    }
    
    public static class Level2ValueDataFetcher implements DataFetcher<DataFetcherResult<String>> {

        @Override
        public DataFetcherResult<String> get(final DataFetchingEnvironment environment) throws Exception {
            
            System.out.println("level2: ");
            System.out.println("environment.getSource(): " + environment.getSource());
            System.out.println("environment.getLocalContext(): " + environment.getLocalContext());
            
            return DataFetcherResult.<String>newResult().data("context was: " + environment.getLocalContext()).build();
        }
    }

    private GraphQL initGraphQl() {
        SchemaParser schemaParser = new SchemaParser();
        TypeDefinitionRegistry typeDefinitionRegistry = schemaParser.parse(SCHEMA);

        RuntimeWiring runtimeWiring = RuntimeWiring.newRuntimeWiring()
                .type("Query", builder -> builder.dataFetcher("rootQuery", new RootQueryDataFetcher()))
                .type("Level1", builder -> builder.dataFetcher("value", new Level1DataFetcher()))
                .type("Level2", builder -> builder.dataFetcher("value", new Level2ValueDataFetcher()))
                .build();

        SchemaGenerator schemaGenerator = new SchemaGenerator();
        GraphQLSchema graphQLSchema = schemaGenerator.makeExecutableSchema(typeDefinitionRegistry, runtimeWiring);

        return GraphQL.newGraphQL(graphQLSchema).build();
    }

    @Test
    public void testLoadCowOnly() {
        GraphQL graphQL = initGraphQl();

        /* execute query for hello (without parameters) */
        ExecutionResult executionResult = graphQL.execute("{rootQuery{value{value}}}");
        ExecutionResultAssert.assertThat(executionResult)
                .isDataEqualTo("{rootQuery={value={value=context was: localContextCreatedByRoot}}}");
    }
  
}
