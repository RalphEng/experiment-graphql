package de.humanfork.experiment.graphql;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;

import de.humanfork.experiment.graphql.EnumTest.Planet;
import de.humanfork.experiment.graphql.asserts.ExecutionResultAssert;
import de.humanfork.experiment.graphql.scalar.LocalDateScalar;
import graphql.GraphQL;
import graphql.schema.GraphQLSchema;
import graphql.schema.StaticDataFetcher;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.SchemaGenerator;
import graphql.schema.idl.SchemaParser;
import graphql.schema.idl.TypeDefinitionRegistry;

public class ScalarTest {

    public static class Demo {
        private LocalDate date;

        public Demo() {}
        
        public Demo(LocalDate date) {
            this.date = date;
        }

        public LocalDate getDate() {
            return date;
        }
    }
        
    public GraphQL buildGraphQlApi() {
        // @formatter:off
        String schema = 
                  "scalar LocalDate           \n"
                + "                           \n"                
                + "type Query {               \n"
                + "  getDemo : Demo!          \n"
                + "}                          \n"
                + "                           \n"
                + "type Demo {                \n"
                + "  date : LocalDate!        \n"
                + "}                            ";
        // @formatter:on

        SchemaParser schemaParser = new SchemaParser();        
        TypeDefinitionRegistry typeDefinitionRegistry = schemaParser.parse(schema);

        //see https://github.com/graphql-java/graphql-java-extended-scalars
        RuntimeWiring runtimeWiring = RuntimeWiring.newRuntimeWiring()
                .scalar(LocalDateScalar.LOCAL_DATE)
                .type("Query",                        
                        builder -> builder.dataFetcher("getDemo", new StaticDataFetcher(new Demo(LocalDate.of(2019, 10, 22)))))
                .build();

        SchemaGenerator schemaGenerator = new SchemaGenerator();
        GraphQLSchema graphQLSchema = schemaGenerator.makeExecutableSchema(typeDefinitionRegistry, runtimeWiring);

        return GraphQL.newGraphQL(graphQLSchema).build();
    }
    
    @Test
    public void testGetLocalDate() {
        ExecutionResultAssert.assertThat(buildGraphQlApi().execute("query {getDemo{date}}"))
                .isDataEqualTo("{getDemo={date=2019-10-22}}");
    }
    
}
