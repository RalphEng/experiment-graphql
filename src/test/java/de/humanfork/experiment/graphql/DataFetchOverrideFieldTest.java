package de.humanfork.experiment.graphql;

import java.util.List;

import org.junit.jupiter.api.Test;

import de.humanfork.experiment.graphql.DataFetchInsertTest.Cow;
import de.humanfork.experiment.graphql.DataFetchInsertTest.CowDataFetcher;
import de.humanfork.experiment.graphql.DataFetchInsertTest.Stable;
import de.humanfork.experiment.graphql.DataFetchInsertTest.StableDataFetcher;
import de.humanfork.experiment.graphql.asserts.ExecutionResultAssert;
import graphql.ExecutionResult;
import graphql.GraphQL;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.GraphQLSchema;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.SchemaGenerator;
import graphql.schema.idl.SchemaParser;
import graphql.schema.idl.TypeDefinitionRegistry;

public class DataFetchOverrideFieldTest {

    // @formatter:off
    private static final String SCHEMA =
             "type Query{cows: [Cow]!} \n"
           + "type Cow {businessId:ID!, name:String!} \n"
           + "type Stable {id:ID!, name:String!} \n";
    // @formatter:on

    public static class BusinessId {
        private final int bid;

        public BusinessId(int bid) {
            this.bid = bid;
        }        
        
    }
    
    /** Straight forward domain object. */
    public static class Cow {

        private final BusinessId businessId; 

        @SuppressWarnings("unused")
        private final String name;

        public Cow(final BusinessId businessId, final String name) {
            this.businessId = businessId;
            this.name = name;
        }
    }
    
    public static class CowDataFetcher implements DataFetcher<List<Cow>> {

        private final Cow friggy = new Cow(new BusinessId(1), "Friggy");
        private final Cow birgit = new Cow(new BusinessId(2), "Birgit");

        @Override
        public List<Cow> get(final DataFetchingEnvironment environment) throws Exception {
            return List.of(this.friggy, this.birgit);
        }
    }

    public static class BusinessIdFetcher implements DataFetcher<Integer> {

        @Override
        public Integer get(final DataFetchingEnvironment environment) throws Exception {            
            Cow parent = environment.getSource();
            return parent.businessId.bid;
        }
    }
    
    private GraphQL initGraphQl() {
        SchemaParser schemaParser = new SchemaParser();
        TypeDefinitionRegistry typeDefinitionRegistry = schemaParser.parse(SCHEMA);

        RuntimeWiring runtimeWiring = RuntimeWiring.newRuntimeWiring()
                .type("Query", builder -> builder.dataFetcher("cows", new CowDataFetcher()))
                .type("Cow", builder -> builder.dataFetcher("businessId", new BusinessIdFetcher())).build();

        SchemaGenerator schemaGenerator = new SchemaGenerator();
        GraphQLSchema graphQLSchema = schemaGenerator.makeExecutableSchema(typeDefinitionRegistry, runtimeWiring);

        return GraphQL.newGraphQL(graphQLSchema).build();
    }

    @Test
    public void testLoadCowOnly() {
        GraphQL graphQL = initGraphQl();

        /* execute query for hello (without parameters) */
        ExecutionResult executionResult = graphQL.execute("{cows{businessId, name}}");
        ExecutionResultAssert.assertThat(executionResult)
                .isDataEqualTo("{cows=[{businessId=1, name=Friggy}, {businessId=2, name=Birgit}]}");
    }
}
