package de.humanfork.experiment.graphql.cowdomain;

import java.util.Comparator;

/** Straight forward domain object. */
public class Cow {

    private final int id;

    private final String name;

    public Cow(final int id, final String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public static Comparator<Cow> COW_BY_ID_COMPARATOR = Comparator.<Cow, Integer> comparing(c -> c.getId());
}
