package de.humanfork.experiment.graphql.cowdomain;

/** Domain Concept: Represent the time based relation ship: When a cow resists where. */
public class CowResidence {

    private final Cow cow;

    private final Stable stable;

    private final DateRange dateRange;

    public CowResidence(final Cow cow, final Stable stable, final DateRange dateRange) {
        this.cow = cow;
        this.stable = stable;
        this.dateRange = dateRange;
    }

    public Cow getCow() {
        return this.cow;
    }

    public Stable getStable() {
        return this.stable;
    }

    public DateRange getDateRange() {
        return this.dateRange;
    }
}