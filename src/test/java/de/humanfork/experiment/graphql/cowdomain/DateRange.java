package de.humanfork.experiment.graphql.cowdomain;

import java.time.LocalDate;

/** Value object to express a Date Range with from and to dates. */
public class DateRange {

    /** The form date (included). */
    private final LocalDate from;

    /** The to date (excluded). */
    private final LocalDate to;

    public DateRange(final LocalDate from, final LocalDate to) {
        this.from = from;
        this.to = to;
    }

    public boolean contains(final LocalDate date) {
        return (this.from.isEqual(date) || this.from.isBefore(date)) && this.to.isAfter(date);
    }
}