package de.humanfork.experiment.graphql.cowdomain;

/** Straight forward domain object. */
public class Stable {

    @SuppressWarnings("unused")
    private final int id;

    @SuppressWarnings("unused")
    private final String name;

    public Stable(final int id, final String name) {
        this.id = id;
        this.name = name;
    }

}