package de.humanfork.experiment.graphql.cowdomain;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import de.humanfork.experiment.graphql.util.Pair;

public class CowRepository {

    private final List<Cow> cows;

    private final List<CowResidence> cowResisdence;

    public CowRepository(final List<CowResidence> cowResisdence) {
        this.cowResisdence = new ArrayList<>(cowResisdence);
        this.cows = this.cowResisdence.stream().map(CowResidence::getCow).distinct().collect(Collectors.toList());
    }

    public Stream<Cow> findByAsStream(Predicate<Cow> specification) {
        return this.cows.stream().filter(specification);
    }

    public Stream<Cow> findAllAsStream() {
        return findByAsStream(everyThing -> true);
    }

    public List<Cow> findBy(Predicate<Cow> specification) {
        return findByAsStream(specification).collect(Collectors.toList());
    }

    public List<Cow> findAll() {
        return findAllAsStream().collect(Collectors.toList());
    }

    private Optional<CowResidence> findCowResisdenceByCowAndDate(final Cow cow, final LocalDate resisdendAtDate) {
    // @formatter:off
    return this.cowResisdence.stream()
            .filter(cowResisdence -> cowResisdence.getCow().equals(cow))
            .filter(cowResisdence -> cowResisdence.getDateRange().contains(resisdendAtDate))
            .findAny();
    // @formatter:on
    }

    public Stream<Pair<Cow, Optional<Stable>>> findAllAndStableAtDate(final LocalDate resisdendAtDate) {
        return findAllAsStream().map(cow -> new Pair<>(cow,
                findCowResisdenceByCowAndDate(cow, resisdendAtDate).map(CowResidence::getStable)));
    }
}
