package de.humanfork.experiment.graphql.scalar;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.TemporalAccessor;
import java.util.Objects;
import java.util.function.BiFunction;

import graphql.language.StringValue;
import graphql.schema.Coercing;
import graphql.schema.CoercingParseLiteralException;
import graphql.schema.CoercingParseValueException;
import graphql.schema.CoercingSerializeException;
import graphql.schema.GraphQLScalarType;

public class LocalDateScalar {

    private static final DateTimeFormatter FORMAT = DateTimeFormatter.ISO_LOCAL_DATE;

    public static final GraphQLScalarType LOCAL_DATE = GraphQLScalarType.newScalar().name("LocalDate")
            .description("Local Date in 'yyyy-mm-dd' format").coercing(new Coercing<LocalDate, String>() {

                @Override
                public String serialize(final Object input) throws CoercingSerializeException {
                    Objects.isNull(input);

                    if (input instanceof TemporalAccessor) {
                        return FORMAT.format((TemporalAccessor) input);
                    } else if (input instanceof String) {
                        return FORMAT.format(parseLocalDate((String) input, CoercingSerializeException::new));
                    } else {
                        throw new CoercingSerializeException("Invalid value '" + input + "' for LocalDate");
                    }
                }

                @Override
                public LocalDate parseValue(final Object input) throws CoercingParseValueException {
                    Objects.nonNull(input);

                    if (input instanceof TemporalAccessor) {
                        try {
                            return LocalDate.from((TemporalAccessor) input);
                        } catch (DateTimeException e) {
                            throw new CoercingParseValueException(
                                    "Unable to turn TemporalAccessor `" + input + "` into full date",
                                    e);
                        }
                    } else if (input instanceof String) {
                        return parseLocalDate(((StringValue) input).getValue(), CoercingParseValueException::new);
                    } else {
                        throw new CoercingParseValueException("Invalid value '" + input + "' for LocalDate");
                    }
                }

                @Override
                public LocalDate parseLiteral(final Object input) throws CoercingParseLiteralException {
                    Objects.nonNull(input);

                    if (!(input instanceof StringValue)) {
                        return null;
                    }
                    return parseLocalDate(((StringValue) input).getValue(), CoercingParseLiteralException::new);
                }

                private LocalDate parseLocalDate(final String s,
                        final BiFunction<String, Throwable, RuntimeException> exceptionConstructor) {
                    try {
                        TemporalAccessor temporalAccessor = FORMAT.parse(s);
                        return LocalDate.from(temporalAccessor);
                    } catch (DateTimeParseException e) {
                        throw exceptionConstructor.apply("Invalid ISO-LOCAL-DATE date value : '" + s + "'", e);
                    }
                }

            }).build();
}
