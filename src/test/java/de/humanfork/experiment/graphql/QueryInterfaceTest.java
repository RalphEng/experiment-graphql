package de.humanfork.experiment.graphql;

import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;

import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import de.humanfork.experiment.graphql.asserts.ExecutionResultAssert;
import de.humanfork.experiment.graphql.cowdomain.Cow;
import de.humanfork.experiment.graphql.cowdomain.CowRepository;
import de.humanfork.experiment.graphql.cowdomain.CowResidence;
import de.humanfork.experiment.graphql.cowdomain.DateRange;
import de.humanfork.experiment.graphql.cowdomain.Stable;
import graphql.ExecutionInput;
import graphql.ExecutionResult;
import graphql.GraphQL;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.GraphQLSchema;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.SchemaGenerator;
import graphql.schema.idl.SchemaParser;
import graphql.schema.idl.TypeDefinitionRegistry;

public class QueryInterfaceTest {

    public GraphQL buildGraphQlApi(final CowRepository cowRepository) {
        String schema = "type Query { cowsQuery(filter:CowQueryFilter): [Cow!]! }  \n"
                + "type Cow {id:ID!, name:String}                                  \n"
                + "input CowQueryFilter {                                          \n"
                + "  id:IdFilter                                                   \n"
                + "  name:StringFilter                                             \n"
                + "}                                                               \n"
                + "input IdFilter {                                                \n"
                + "  eq:Int!                                                       \n"
                + "}                                                               \n"
                + "input StringFilter {                                            \n"
                + "  eq:String!                                                    \n"
                + "}                                                               \n";

        SchemaParser schemaParser = new SchemaParser();
        TypeDefinitionRegistry typeDefinitionRegistry = schemaParser.parse(schema);

        RuntimeWiring runtimeWiring = RuntimeWiring.newRuntimeWiring()
                .type("Query", builder -> builder.dataFetcher("cowsQuery", new CowDataFetcher(cowRepository)))
                .build();

        SchemaGenerator schemaGenerator = new SchemaGenerator();
        GraphQLSchema graphQLSchema = schemaGenerator.makeExecutableSchema(typeDefinitionRegistry, runtimeWiring);

        return GraphQL.newGraphQL(graphQLSchema).build();
    }

    public static class IdFilter {
        /** The expected Id. */
        public int eq;

        public IdFilter() {
        }

        public IdFilter(int eq) {
            this.eq = eq;
        }

        Predicate<Integer> toSpecification() {
            return value -> value.equals(this.eq);
        }

        @Override
        public String toString() {
            return "IdFilter [eq=" + eq + "]";
        }

    }

    public static class StringFilter {

        /** The expected String */
        public String eq;

        public StringFilter() {
        }

        public StringFilter(String eq) {
            this.eq = eq;
        }

        Predicate<String> toSpecification() {
            return value -> value.equals(this.eq);
        }

        @Override
        public String toString() {
            return "StringFilter [eq=" + eq + "]";
        }
    }

    public static class CowQueryFilter {

        public IdFilter id;

        public StringFilter name;

        public CowQueryFilter() {
        }

        public IdFilter getId() {
            return id;
        }

        public CowQueryFilter setId(IdFilter id) {
            this.id = id;
            return this;
        }

        public StringFilter getName() {
            return name;
        }

        public CowQueryFilter setName(StringFilter name) {
            this.name = name;
            return this;
        }

        Predicate<Cow> toSpecification() {
            List<Predicate<Cow>> filterPredicates = new ArrayList<>();

            if (this.id != null) {
                filterPredicates.add(c -> this.id.toSpecification().test(c.getId()));
            }
            if (this.name != null) {
                filterPredicates.add(c -> this.name.toSpecification().test(c.getName()));
            }

            // conjunction of all predicates into one
            return filterPredicates.stream().reduce(x -> true, Predicate::and);
        }

        @Override
        public String toString() {
            return "CowQueryFilter [id=" + id + ", name=" + name + "]";
        };
    }

    public static class CowDataFetcher implements DataFetcher<List<Cow>> {

        /**
         * Arguments, even for complex input types, are expressed by graphql-java always as a {@code Map<String, Object>}
         * where the key is the name of the attribute, and the object is a value for primitive input types or a Map (recursive)
         * for complex objects.
         * 
         * graphql-java has not support for conversion of this Map to (custom) Objects.
         * 
         * But there are several libraries that do such a conversion: for example Jackson, Gson, Dozer, BeanUtils.
         * https://stackoverflow.com/questions/16428817/convert-a-mapstring-string-to-a-pojo
         * 
         */
        private final ObjectMapper mapper = new ObjectMapper();

        private final CowRepository cowRepository;

        public CowDataFetcher(final CowRepository cowRepository) {
            this.cowRepository = cowRepository;
        }

        @Override
        public List<Cow> get(final DataFetchingEnvironment environment) throws Exception {
            
            @SuppressWarnings("unchecked")
            Optional<Map<String, Object>> filterArgumentMap = Optional
                    .ofNullable((Map<String, Object>) environment.getArgument("filter"));            
            
            /** Convert a Map<String, Object> (the value can be a primitive or a Map again) to an instance of CowQueryFilter.*/
            Optional<CowQueryFilter> filter = filterArgumentMap
                    .map(argsMap -> mapper.convertValue(argsMap, CowQueryFilter.class));
            
            Predicate<Cow> specification = filter.map(CowQueryFilter::toSpecification).orElse(everyCow -> true);

            return this.cowRepository.findBy(specification);
        }
    }

    private static final LocalDate NOW = LocalDate.of(2019, Month.AUGUST, 2);

    private static CowRepository buildCowRepository() {
        Cow friggy = new Cow(1, "Friggy");
        Cow birgit = new Cow(2, "Birgit");

        Stable dresden = new Stable(1, "Dresden");
        Stable freital = new Stable(2, "Freital");
        Stable hamburg = new Stable(2, "Hamburg");

        List<CowResidence> cowResisdence = Arrays.asList(
                new CowResidence(friggy, hamburg, new DateRange(NOW.minusDays(10), NOW.minusDays(5))),
                new CowResidence(friggy, freital, new DateRange(NOW.minusDays(5), NOW)),
                new CowResidence(friggy, dresden, new DateRange(NOW, NOW.plusDays(10))),

                new CowResidence(birgit, dresden, new DateRange(NOW.minusDays(10), NOW.minusDays(5))),
                new CowResidence(birgit, freital, new DateRange(NOW.minusDays(5), NOW)),
                new CowResidence(birgit, hamburg, new DateRange(NOW, NOW.plusDays(10))));

        return new CowRepository(cowResisdence);
    }

    @Test
    public void testInputViaVariables() {
        ExecutionInput executionInput = ExecutionInput
                .newExecutionInput("query cows($filter:CowQueryFilter) {    \n"
                        + "   cowsQuery(filter:$filter) {                   \n"
                        + "     id,                                         \n"
                        + "     name,                                       \n"
                        + "   }                                             \n"
                        + "}                                                \n")
                .variables(Map.of("filter", Map.of("name", Map.of("eq", "Friggy"))))
                .build();

        ExecutionResult result = buildGraphQlApi(buildCowRepository()).execute(executionInput);
        ExecutionResultAssert.assertThat(result).isDataEqualTo("{cowsQuery=[{id=1, name=Friggy}]}");
    }

    @Test
    public void testInputViaParameters() {
        ExecutionInput executionInput = ExecutionInput
                .newExecutionInput("query {                                 \n"
                        + "   cowsQuery(filter:{name:{eq:\"Friggy\"}}) {                   \n"
                        + "     id,                                         \n"
                        + "     name,                                       \n"
                        + "   }                                             \n"
                        + "}                                                \n")
                .build();

        ExecutionResult result = buildGraphQlApi(buildCowRepository()).execute(executionInput);
        ExecutionResultAssert.assertThat(result).isDataEqualTo("{cowsQuery=[{id=1, name=Friggy}]}");
    }

    @Test
    public void testNoFilter() {
        ExecutionInput executionInput = ExecutionInput
                .newExecutionInput("query cows($filter:CowQueryFilter) {    \n"
                        + "   cowsQuery(filter:$filter) {                   \n"
                        + "     id,                                         \n"
                        + "     name,                                       \n"
                        + "   }                                             \n"
                        + "}                                                \n")
                .build();

        ExecutionResult result = buildGraphQlApi(buildCowRepository()).execute(executionInput);
        ExecutionResultAssert.assertThat(result)
                .isDataEqualTo("{cowsQuery=[{id=1, name=Friggy}, {id=2, name=Birgit}]}");
    }

    @Test
    public void testIdFilter() {
        ExecutionInput executionInput = ExecutionInput
                .newExecutionInput("query cows($filter:CowQueryFilter) {    \n"
                        + "   cowsQuery(filter:$filter) {                   \n"
                        + "     id,                                         \n"
                        + "     name,                                       \n"
                        + "   }                                             \n"
                        + "}                                                \n")
                .variables(Map.of("filter", Map.of("id", Map.of("eq", "2"))))
                .build();

        ExecutionResult result = buildGraphQlApi(buildCowRepository()).execute(executionInput);
        ExecutionResultAssert.assertThat(result)
                .isDataEqualTo("{cowsQuery=[{id=2, name=Birgit}]}");
    }
    
    @Test
    public void testNameFilter() {
        ExecutionInput executionInput = ExecutionInput
                .newExecutionInput("query cows($filter:CowQueryFilter) {    \n"
                        + "   cowsQuery(filter:$filter) {                   \n"
                        + "     id,                                         \n"
                        + "     name,                                       \n"
                        + "   }                                             \n"
                        + "}                                                \n")
                .variables(Map.of("filter", Map.of("name", Map.of("eq", "Birgit"))))
                .build();

        ExecutionResult result = buildGraphQlApi(buildCowRepository()).execute(executionInput);
        ExecutionResultAssert.assertThat(result)
                .isDataEqualTo("{cowsQuery=[{id=2, name=Birgit}]}");
    }

}
