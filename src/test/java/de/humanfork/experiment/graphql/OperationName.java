package de.humanfork.experiment.graphql;

import org.junit.jupiter.api.Test;

import de.humanfork.experiment.graphql.asserts.ExecutionResultAssert;
import graphql.GraphQL;
import graphql.schema.GraphQLSchema;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.SchemaGenerator;
import graphql.schema.idl.SchemaParser;
import graphql.schema.idl.TypeDefinitionRegistry;

public class OperationName {

    public GraphQL buildGraphQlApi() {
        String schema = "type Query {getGreeting(lang:String): String! }";

        SchemaParser schemaParser = new SchemaParser();
        TypeDefinitionRegistry typeDefinitionRegistry = schemaParser.parse(schema);

        RuntimeWiring runtimeWiring = RuntimeWiring.newRuntimeWiring()
                .type("Query", builder -> builder.dataFetcher("getGreeting", environment -> "world")).build();

        SchemaGenerator schemaGenerator = new SchemaGenerator();
        GraphQLSchema graphQLSchema = schemaGenerator.makeExecutableSchema(typeDefinitionRegistry, runtimeWiring);

        return GraphQL.newGraphQL(graphQLSchema).build();
    }

    /**
     * shorthand syntax: both the query keyword and the query name are omitted
     */
    @Test
    public void testShorthandSyntax() {
        ExecutionResultAssert.assertThat(buildGraphQlApi().execute("{getGreeting(lang:\"de\")}"))
                .isDataEqualTo("{getGreeting=world}");
    }

    /**
     * explicit "query" keyword
     */
    @Test
    public void testExpicitOperationTypeKeyword() {
        /* execute query for not existing id*/
        ExecutionResultAssert.assertThat(buildGraphQlApi().execute("query {getGreeting(lang:\"de\")}"))
                .isDataEqualTo("{getGreeting=world}");
    }
    
    /**
     * explicit "query" keyword and query-name
     * The query name is a client defined alias, it is not defined in the schema nor a element of the result
     */
    @Test
    public void testExpicitQueryName() {
        /* execute query for not existing id*/
        ExecutionResultAssert.assertThat(buildGraphQlApi().execute("query clientDefinedQueryName {getGreeting(lang:\"de\")}"))
                .isDataEqualTo("{getGreeting=world}");
    }
}
